<?php

/**
 * Plugin Name:       Chatbot API
 * Plugin URI:        https://www.virtuelegemeenteassistent.nl/
 * Description:       Een plugin die een API ter beschikking stelt voor het bevragen van utters.
 * Version:           2.2.4
 * Author:            Frank
 * License:           GPL-3.0
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain:       utter-api
 * Domain Path:       /
 */


 /* http://localhost/wp-json/chatbot/v2/utter*/



// live zetten: response_text naar response opnoemen
// where allow_override = 1 toevoegen aan query voor table

function chatbot_utter_with_buttons($data) {
    $query = $GLOBALS['wpdb']->prepare(
        "select id, utter from chatbot_utters where utter=%s limit 1;",
        $data['utter']
    );
    $result_utters = $GLOBALS['wpdb']->get_results( $query );

    if (count($result_utters) == 0){
        $result = (object) [
            'utter' => '',
            'response' => '',
            'error' => 'not found',
          ];
        return $result;
    }
    
    $utter_id = $result_utters[0]->id;
    $utter = $result_utters[0]->utter;
    $query = $GLOBALS['wpdb']->prepare(
        "select id, response, image, button1_title, button1_payload, button2_title, button2_payload, button3_title, button3_payload, button4_title, button4_payload from chatbot_responses where chatbot_utters_id=%d limit 100;",
        $utter_id 
    );
    $result = $GLOBALS['wpdb']->get_results( $query );

    foreach($result as $row) {

        $buttons = array(); 
        if (!empty($row->button1_title))
        {
            $buttons[] = (object) [
                'title' => $row->button1_title,
                'payload' => $row->button1_payload,
            ];
        }

        if (!empty($row->button2_title))
        {
            $buttons[] = (object) [
                'title' => $row->button2_title,
                'payload' => $row->button2_payload,
            ];
        }

        if (!empty($row->button3_title))
        {
            $buttons[] = (object) [
                'title' => $row->button3_title,
                'payload' => $row->button3_payload,
            ];
        }

        if (!empty($row->button4_title))
        {
            $buttons[] = (object) [
                'title' => $row->button4_title,
                'payload' => $row->button4_payload,
            ];
        }

        $result_row = (object) [
            'text' => $row->response,
            'image' => $row->image,
            'buttons' => $buttons,
        ];

        $results_rows[] = $result_row;
    }
    
    $results = (object) [
        'utter' => $utter,
        'response' => $results_rows,
    ];

    return $results;
}

function chatbot_version($data) {
    $result = (object) [
        'version' => '2.2.4',
    ];

    return $result;
}


function chatbot_utter($data) {
    $query = $GLOBALS['wpdb']->prepare(
        "select id, utter, response from chatbot_utters where utter=%s limit 1;",
        $data['utter']
    );
    $result_utters = $GLOBALS['wpdb']->get_results( $query );

    if (count($result_utters) == 0){
        $result = (object) [
            'utter' => '',
            'response' => '',
            'error' => 'not found',
          ];
        return $result;
    }
    
    $result = (object) [
        'text' => $result_utters[0]->response,
        'image' => null,
        'buttons' => (object) [],
        'attachments' => (object) [],
        'elements' => (object) [],
    ];

    return $result;
}

function import_utters($data) {
    
    $url = get_option('utter_api_option_gem_content_api_url');
    $request = wp_remote_get( $url );

    if( is_wp_error( $request ) ) {
        $result = (object) [
            'result' => 'kon geen connectie maken met API URL',
        ];
        return $result;
    }

    $body = wp_remote_retrieve_body( $request );
    $data = json_decode( $body );

    /*    $data = json_decode( '[
        {"utter": "utter_greet22", "response": "Hoi, ik ben Gem"},
        {"utter": "utter_ask_affirmation", "response": "Bedoel je ..."}
    ]' );
*/
    if( ! empty( $data ) ) {
        // disable all overrides

        $query = $GLOBALS['wpdb']->prepare( 'UPDATE chatbot_utters SET allow_override = %d', 0);
        $GLOBALS['wpdb']->query($query);

        foreach( $data as $utter ) {
            $query = $GLOBALS['wpdb']->prepare(
                "select id from chatbot_utters where utter=%s limit 1;",
                $utter->utter
            );

            $result_utters = $GLOBALS['wpdb']->get_row ( $query );
            if ($result_utters == NULL) {
                $table = 'chatbot_utters';
                $data = array('utter' => $utter->utter, 'response' => $utter->response, 'description' => '', 'allow_override' => 1);
                $format = array('%s','%s', '%s', '%d');
                $GLOBALS['wpdb']->insert($table,$data,$format);
            }
            else {
                $data = array ('allow_override' => 1 ); 
                $format = array( '%d' );  
                $where = array ( 'id' => $result_utters->id ); 
                $where_format = array ('%d');  
                $GLOBALS['wpdb']->update('chatbot_utters', $data, $where, $format, $where_format );
            }
        }
    }
    
     

    $result = (object) [
        'result' => 'update gelukt',
    ];

    return $result;
}

function myplugin_register_settings() {
    add_option( 'utter_api_option_name', '');
    register_setting( 'utter_api_options_group', 'utter_api_option_gem_content_api_url', 'utter_api_callback' );
 }
 add_action( 'admin_init', 'myplugin_register_settings' );

 function myplugin_register_options_page() {
    add_options_page('Utter API settings', 'Utter API', 'manage_options', 'utter_api', 'utter_api_options_page');
  }
  add_action('admin_menu', 'myplugin_register_options_page');
  


    add_action('rest_api_init', function() {
    register_rest_route('chatbot/v2', 'import', [
        'methods' => 'GET',
        'callback' =>  'import_utters'
    ]);

});

add_action('rest_api_init', function() {
    register_rest_route('chatbot/v1', 'utter/(?P<utter>\w+)', [
        'methods' => 'GET',
        'callback' =>  'chatbot_utter_with_buttons'
    ]);

});


add_action('rest_api_init', function() {
    register_rest_route('chatbot/v2', 'utter/(?P<utter>\w+)', [
        'methods' => 'GET',
        'callback' =>  'chatbot_utter'
    ]);

});

add_action('rest_api_init', function() {
    register_rest_route('chatbot', 'version', [
        'methods' => 'GET',
        'callback' =>  'chatbot_version'
    ]);

});

?>

<?php function utter_api_options_page()
  {
  ?>
    <div>
    <?php screen_icon(); ?>
    <h2>Utter API settings</h2>
    <form method="post" action="options.php">
    <?php settings_fields( 'utter_api_options_group' ); ?>
    <table>
    <tr valign="top">
    <th scope="row"><label for="utter_api_option_gem_content_api_url">Gem content API url</label></th>
    <td><input type="text" id="utter_api_option_gem_content_api_url" name="utter_api_option_gem_content_api_url" value="<?php echo get_option('utter_api_option_gem_content_api_url'); ?>" /></td>
    </tr>
    </table>
    <?php  submit_button(); ?>
    </form>
    </div>
  <?php
} ?>
